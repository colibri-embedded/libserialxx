#include <stdio.h>
#include <iostream>
#include <string>
#include <thread>
#include <serialxx/serial.hpp>
#include <CLI/CLI.hpp>

using namespace serialxx;

void show_version(size_t)
{
	std::cout << SERIALXX_PACKAGE_STRING << std::endl;
	throw CLI::Success();
}

int main(int argc, char **argv)
{
	CLI::App app{"sercomxx"};

	std::string port = "/dev/ttyUSB0";
	unsigned baud_rate = 9600;
	bool interactive = false;
	bool send_lines = false;
	bool only_cr = false;
	bool only_lf = false;
	bool only_crlf = true;
	bool no_line_end = false;
	bool hex_mode = false;
	bool invert_rts = false;
	RS485mode rs485_mode = RS485mode::OFF;

	std::vector<std::pair<std::string, RS485mode>> map{
        {"off", RS485mode::OFF}, {"hw", RS485mode::HARDWARE}, {"sw", RS485mode::SOFTWARE}};

	app.add_flag_function("-v,--version", show_version, "Show version");

	app.add_option("-p,--port", port, "Serial port [default: " + port + "]");
	app.add_option("-b,--baud", baud_rate, "Serial baudrate [default: " + std::to_string(baud_rate) + "]");
	app.add_flag("-l,--send-lines", send_lines, "Send data only when ENTER is pressed [default: no]");
	app.add_flag("--cr", only_cr, "Use CR as line end [default: no]");
	app.add_flag("--lf", only_lf, "Use CR as line end [default: no]");
	app.add_flag("--crlf", only_crlf, "Use CR+LF as line end [default: yes]");
	app.add_flag("-n,--nolf", no_line_end, "Use CR+LF as line end [default: no]");
	app.add_flag("-i,--interactive", interactive, "Interactive terminal [default: no]");
	app.add_flag("-x,--hex", hex_mode, "Input data is considered as hex [default: no]");
	app.add_option("--rs485", rs485_mode, "Set RS485 mode (off|hw|sw) [default: off]")
		->transform(CLI::CheckedTransformer(map, CLI::ignore_case));
	app.add_flag("--rts-inv", invert_rts, "Invert RTS polarity for RS485 mode [default: no]");

	CLI11_PARSE(app, argc, argv);
	if(no_line_end) {
		only_cr = only_lf = only_crlf = false;
	}
	// Hex-mode automaticaly turns send-lines mode as well
	if(hex_mode) {
		send_lines = true;
	}

	auto ser = thread_safe::Serial(port, baud_rate);

	if(rs485_mode != RS485mode::OFF) {
		ser.setRS485Mode(rs485_mode);
		ser.setRtsPolarity(!invert_rts);
	}

	try {
		if(!ser.open()) {
			std::cerr << "Cannot open " << port << std::endl;
			return 1;
		}
	} catch(RS485Failure &e) {
		std::cerr << "Faild to open port " << port << " in RS485 mode: " << e.what() << std::endl;
		return 1;
	} catch(std::exception &e) {
		std::cerr << "Faild to open port " << port << ": " << e.what() << std::endl;
		return 1;
	}


	std::cout << "Port " << port << " @ " << std::to_string(baud_rate) << "\n";
	std::cout << "To Quit press CTRL+D\n";

	if(interactive) {

		if(!send_lines)
			system("stty raw");

		std::thread receiver([&ser]{
			while(1) {
				try {
					char c = ser.readByte(100);
					std::cout << c << std::flush;
				} catch(serialxx::NotOpen &e) {
					std::cout << "closed\n" << std::flush;
					return;
				} catch(serialxx::ReadTimeout &e) {
					// std::cout << "timeout\n" << std::flush;
				} catch(std::exception &e) {
					// std::cerr << "Exception: " << e.what() << std::endl << std::flush;
					return;
				}
			}
		});



		while (!std::cin.eof())
		{
			if(send_lines) {
				char line[1024];
				std::cin.getline(line, 1024);

				if(hex_mode) {
					// TODO:
				}

				// Add line end
				auto len = strlen(line);
				if(only_cr || only_crlf) {
					line[len++] = '\r';
				}
				if(only_lf || only_crlf) {
					line[len++] = '\n';
				}
				line[len] = '\0';
				// Send data
				ser.write(line, len);
			} else {
				int c = getchar();
				if(c == 0x04) { // CTRL+D
					std::cout << "aborting...\n";
					break;
				}
				ser.writeByte((char)c);
			}
		}

		if(!send_lines)
			system("stty cooked");

		ser.close();
		receiver.join();
	} else {
		ser.close();
	}

	return 0;
}