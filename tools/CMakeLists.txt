include_directories("${3RDPARTY_SOURCE_DIR}")

if(BUILD_SHARED)
	set(SERIALXX_LIB serialxx-shared)
endif()

if(BUILD_STATIC)
	set(SERIALXX_LIB serialxx-static)
endif()

add_executable(sercomxx
	sercomxx.cpp 
	)
target_link_libraries(
	sercomxx
	${SERIALXX_LIB})