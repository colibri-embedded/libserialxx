#include <serialxx/platform/unix.hpp>

#include <ctype.h>
#include <stdlib.h>
#include <cstring>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/serial.h>

#include <chrono>
#include <iostream>

#define TCGETS2_custom  0x802C542A
#define TCSETS2_custom  0x402C542B
#define BOTHER  010000

#define VTIME_SCALE 100

using namespace serialxx;

/**
 * @brief Standard baud rates map structure
 */
struct baudrate_map {
    unsigned baud;
    unsigned value;
};

/**
 * @brief Map of standard baud rates and their defined value
 */
struct baudrate_map baudmap[] = 
{
    /* standard rates */
    {B50, 50},
    {B75, 75},
    {B110, 110},
    {B134, 134},
    {B150, 150},
    {B200, 200},
    {B300, 300},
    {B600, 600},
    {B1200, 1200},
    {B1800, 1800},
    {B2400, 2400},
    {B4800, 4800},
    {B9600, 9600},
    {B19200, 19200},
    {B38400, 38400},
    {B57600, 57600},
    {B115200, 115200},
    {B230400, 230400},
    /* extended rates */
    {B460800, 460800},
    {B500000, 500000},
    {B576000, 576000},
    {B921600, 921600},
    //{B1000000,1000000},
    {B1152000, 1152000},
    {B1500000, 1500000},
    {B2000000, 2000000},
    {B2500000, 2500000},
    {B3000000, 3000000},
    {B3500000, 3500000},
    {B4000000, 4000000},
    
    {0,0}
};

/**
 * Set non-standard baurdate
 *
 * @param fd   File descriptor
 * @param baud Baud rate
 *
 * @returns true on success, false on failure
 */
bool set_custom_baudrate(int fd, unsigned baud)
{
	unsigned buf[64];
	int r;

	r = ioctl(fd, TCGETS2_custom, buf);
	if(r) {
		return false;
	}

	// set custom speed
	buf[2] &= ~CBAUD;
	buf[2] |= BOTHER;
	buf[9] = buf[10] = baud;

	r = ioctl(fd, TCSETS2_custom, buf);
	if(r) {
		return false;
	}

	return true;
}

/**
 * Set interface attributes
 *
 * @param fd     File descriptor
 * @param baud   Standard baud rate
 * @param parity Parity value
 *
 * @returns true on success, false on failure
 */
bool set_interface_attribs (int fd, unsigned baud, unsigned parity, unsigned timeout)
{
    struct termios tty;
    std::memset (&tty, 0, sizeof tty);
    
    if (tcgetattr (fd, &tty) != 0)
    {
        // fprintf(stderr, "error %d from tcgetattr", errno);
        return true;
    }
    
    // output speed
    cfsetospeed (&tty, baud);
    // input speed
    cfsetispeed (&tty, baud);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = timeout;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        // fprintf(stderr, "error %d from tcsetattr", errno);
        return false;
    }
        
    return true;
}

/**
 * Set interface vmin and timeout values
 *
 * @param fd   		File descriptor
 * @param vmin 		VMIN terminal value
 * @param timeout 	Read timeout
 *
 * @returns true on success, false on failure
 */
bool set_blocking (int fd, unsigned vmin, unsigned timeout)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        // fprintf(stderr, "error %d from tggetattr", errno);
        return false;
    }

    //tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VMIN] = vmin;
    tty.c_cc[VTIME] = timeout;            // 5 = 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0) {
    	// fprintf(stderr, "error %d setting term attributes", errno);
    	return false;
    }

    return true;
}

Serial::Serial(const std::string& portName, unsigned baudRate, unsigned timeout)
	: SerialBase(portName, baudRate, timeout)
	, fd(0)
{
}

Serial::Serial()
    : Serial("")
{
}

Serial::Serial(const Serial& s)
    : SerialBase(s)
    , fd(s.fd)
{

}

Serial::~Serial()
{
	if(_isOpen) close();
}

bool Serial::open()
{
	unsigned baud = 0, custom_baudrate = _baudRate;

    if(_portName.empty()) {
        return false;
    }

	for(auto &bm : baudmap)
	{
		if(bm.value == _baudRate) {
			baud = bm.baud;
			custom_baudrate = 0;
			break;
		}
	}

    fd = ::open (_portName.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        //fprintf(stderr, "error %d opening %s: %s", errno, portname.c_str(), strerror (errno));
    	throw OpenFailed(ERR_MSG_CANNOT_OPEN_PORT);
    }

    // VTIME is expressed in 1/10th of a second
    unsigned timeout = _timeout / VTIME_SCALE;

    if(custom_baudrate)
    {
        if(not set_interface_attribs (fd, B38400, 0, timeout) ) {
        	throw OpenFailed(ERR_MSG_FD_ATTR_FAIL);
        }

        if(not set_custom_baudrate(fd, custom_baudrate) ) {
        	throw OpenFailed(ERR_MSG_INVALID_BAUD_RATE);
        }

        // set blocking with 5sec timeout
        /*if(not set_blocking (fd, 0, timeout) ) {
        	throw OpenFailed(ERR_MSG_FD_ATTR_FAIL);
        }*/
    }
    else
    {
        if(not set_interface_attribs (fd, baud, 0, timeout) ) {
        	throw OpenFailed(ERR_MSG_INVALID_BAUD_RATE);
        }
        // set blocking with 5sec timeout
        /*if(not set_blocking (fd, 0, timeout) ) {
        	throw OpenFailed(ERR_MSG_FD_ATTR_FAIL);
        }*/
    }

    ::ioctl(fd, TCFLSH, 2); // flush both

    /* RS485 */
    if(_rs485mode == RS485mode::HARDWARE) {
        struct serial_rs485 rs485conf = {0,0,0,0,0,0,0,0};

        /* Enable RS485 mode: */
        rs485conf.flags |= SER_RS485_ENABLED;
        if(_rs485RtsPolarity) {
            /* Set logical level for RTS pin equal to 1 when sending: */
            rs485conf.flags |= SER_RS485_RTS_ON_SEND;
            /* Set logical level for RTS pin equal to 0 after sending: */
            rs485conf.flags &= ~(SER_RS485_RTS_AFTER_SEND);
        } else {
            /* Set logical level for RTS pin equal to 0 when sending: */
            rs485conf.flags &= ~(SER_RS485_RTS_ON_SEND);
            /* Set logical level for RTS pin equal to 1 after sending: */
            rs485conf.flags |= SER_RS485_RTS_AFTER_SEND;
        }
        
        /* Set rts delay before send */
        rs485conf.delay_rts_before_send = _rs485DelayBeforeSend;
        /* Set rts delay after send */
        rs485conf.delay_rts_after_send = _rs485DelayAfterSend;

        /* Set this flag if you want to receive data even while sending data */
        // rs485conf.flags |= SER_RS485_RX_DURING_TX;

        if (ioctl (fd, TIOCSRS485, &rs485conf) < 0) {
            /* Error handling. See errno. */
            throw RS485Failure( strerror(errno) );
        }
    }

	_isOpen = true;

	return true;
}

bool Serial::isRS485Supported()
{
    return true;
}

void Serial::setRTS(bool value) 
{
    if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

    int RTS_flag;
    RTS_flag = TIOCM_RTS;
    if(value) {
        // Set RTS pin
        ::ioctl(fd, TIOCMBIS, &RTS_flag); 
    } else {
        // Clear RTS pin
        ::ioctl(fd, TIOCMBIC, &RTS_flag); 
    }
}


void Serial::close()
{
	if(_isOpen) {
		::close(fd);
        //::ioctl(fd, TCFLSH, 0); // flush receive
        //::ioctl(fd, TCFLSH, 1); // flush transmit
        //::ioctl(fd, TCFLSH, 2); // flush both
        flush();
		_isOpen = false;
		fd = 0;
	}
}

void Serial::abort()
{
    std::unique_lock<std::mutex> guard(m_abort);
    ::ioctl(fd, TIOCSTI, "\n");
    _abort = true;
    // ::close(fd);
}

void Serial::flush()
{
    if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

    ::tcdrain(fd);
}

size_t Serial::write(const char* data, size_t count)
{
	if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

    if(_rs485mode == RS485mode::SOFTWARE) {
        setRTS(_rs485RtsPolarity);
        // TODO: delay before send
    }

    ssize_t result = ::write(fd, data, count);

    if(_rs485mode == RS485mode::SOFTWARE) {
        flush();
        // TODO: delay after send
        setRTS(!_rs485RtsPolarity);
    }

	return result;
}


size_t Serial::write(const std::string& data)
{
	if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

	return write(data.c_str(), data.length() );
}

size_t Serial::writeLine(const std::string &data, const std::string& eol)
{
    if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);    

    size_t len0 = write(data.c_str(), data.length() );
    return len0 + write(eol.c_str(), eol.length() );
}

size_t Serial::writeByte(const char byte)
{
	if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

	char data[] = {byte};
	return write(data, 1);
}

size_t Serial::read(char* data, size_t numberOfBytes, size_t msTimeout)
{
	if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

	size_t elapsed_ms = 0;
	ssize_t read_result = 0;

    size_t number_of_bytes_read = 0;
    size_t number_of_bytes_remaining = std::max((int)numberOfBytes, 1);

    std::chrono::high_resolution_clock::duration entry_time;
    std::chrono::high_resolution_clock::duration current_time;
    std::chrono::high_resolution_clock::duration elapsed_time;

    entry_time = std::chrono::high_resolution_clock::now().time_since_epoch();

    while (number_of_bytes_remaining > 0) {
 		// If insufficient space remains in the buffer, exit the loop and return .
        if (number_of_bytes_read == numberOfBytes)
        {
        	std::cout << "not enough space\n";
            break;
        }

        std::unique_lock<std::mutex> guard(m_abort);
        if(_abort) {
            _abort = false;
            throw Aborted(ERR_MSG_ABORTED);
        }
        guard.unlock();

        read_result = ::read(fd, &data[number_of_bytes_read], number_of_bytes_remaining);

        guard.lock();
        if(_abort) {
            _abort = false;
            throw Aborted(ERR_MSG_ABORTED);
        }
        guard.unlock();

        if (read_result > 0) 
        {
            number_of_bytes_read += read_result;

            if (numberOfBytes != 0)
            {
                number_of_bytes_remaining = numberOfBytes - number_of_bytes_read;

                if (number_of_bytes_remaining == 0)
                {
                    return number_of_bytes_read;
                }
            }
        } 
        else if(read_result == 0 and number_of_bytes_read > 0)  
        {
        	return number_of_bytes_read;
        } 
        else if (read_result < 0 &&
                 errno != EWOULDBLOCK)
        {
            throw std::runtime_error(std::strerror(errno));
        }

		// Obtain the current time.
        current_time = std::chrono::high_resolution_clock::now().time_since_epoch();

        // Calculate the time delta.
        elapsed_time = current_time - entry_time;

        // Calculate the elapsed number of milliseconds.
        elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_time).count();

        // If more than msTimeout milliseconds have elapsed while
        // waiting for data, then we throw a ReadTimeout exception.
        if( (msTimeout > 0 && elapsed_ms > msTimeout) 
         || (msTimeout == 0 && _timeout > 0) )
        {
        	if(number_of_bytes_read == 0)
            	throw ReadTimeout(ERR_MSG_READ_TIMEOUT);

            return number_of_bytes_read;
        }
    }

	return 0;
}

size_t Serial::read(std::string& data, size_t numberOfBytes, size_t msTimeout)
{
    if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);
	if(numberOfBytes == 0) {
		numberOfBytes = _maxStringLength;
	}
	char* buff = new char[numberOfBytes+1];

	size_t len = read(buff, numberOfBytes, msTimeout);

	buff[len] = 0;
	data = buff;
    delete[] buff;

	return data.length();
}

size_t Serial::readLine(std::string &data, size_t msTimeout, const std::string& eol)
{
    if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

    size_t elapsed_ms = 0;
    ssize_t read_result = 0;

    size_t numberOfBytes = _maxStringLength;

    size_t number_of_bytes_read = 0;
    size_t number_of_bytes_remaining = std::max((int)numberOfBytes, 1);
    size_t eol_len = eol.length();

    char *line = new char[numberOfBytes+1];
    line[0] = '\0';

    std::chrono::high_resolution_clock::duration entry_time;
    std::chrono::high_resolution_clock::duration current_time;
    std::chrono::high_resolution_clock::duration elapsed_time;

    entry_time = std::chrono::high_resolution_clock::now().time_since_epoch();

    while (number_of_bytes_remaining > 0) {
        // If insufficient space remains in the buffer, exit the loop and return .
        if (number_of_bytes_read == numberOfBytes)
        {
            std::cout << "not enough space\n";
            break;
        }

        std::unique_lock<std::mutex> guard(m_abort);
        if(_abort) {
            _abort = false;
            delete[] line;
            throw Aborted(ERR_MSG_ABORTED);
        }
        guard.unlock();

        read_result = ::read(fd, &line[number_of_bytes_read], number_of_bytes_remaining);

        guard.lock();
        if(_abort) {
            _abort = false;
            delete[] line;
            throw Aborted(ERR_MSG_ABORTED);
        }
        guard.unlock();

        if (read_result > 0) 
        {
            number_of_bytes_read += read_result;

            if (numberOfBytes != 0)
            {
                number_of_bytes_remaining = numberOfBytes - number_of_bytes_read;

                if (number_of_bytes_remaining == 0)
                {
                    delete[] line;
                    return number_of_bytes_read;
                }
            }

            line[number_of_bytes_read] = '\0';
            if( std::string(reinterpret_cast<const char*>(line + number_of_bytes_read - eol_len), eol_len) == eol) {
                //std::cout << "Found EOL: " << line << "\n";
                data = line;
                delete[] line;
                return number_of_bytes_read;
            } else {
                //std::cout << ">> " << line << "\n";
                //std::cout << "No EOL yet\n";
            }
            // check for eol
            // number_of_bytes_read
        } 
        else if(read_result == 0 and number_of_bytes_read > 0)  
        {
            //data = line;
            //return number_of_bytes_read;
        } 
        else if (read_result < 0 &&
                 errno != EWOULDBLOCK)
        {
            delete[] line;
            throw std::runtime_error(std::strerror(errno));
        }

        // Obtain the current time.
        current_time = std::chrono::high_resolution_clock::now().time_since_epoch();

        // Calculate the time delta.
        elapsed_time = current_time - entry_time;

        // Calculate the elapsed number of milliseconds.
        elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_time).count();

        // If more than msTimeout milliseconds have elapsed while
        // waiting for data, then we throw a ReadTimeout exception.
        if( (msTimeout > 0 && elapsed_ms > msTimeout) 
         || (msTimeout == 0 && _timeout > 0) )
        {
            if(number_of_bytes_read == 0) {
                delete[] line;
                throw ReadTimeout(ERR_MSG_READ_TIMEOUT);
            }
                
            data = line;
            delete[] line;
            return number_of_bytes_read;
        }
    }

    delete[] line;
    return 0;
}

char Serial::readByte(size_t msTimeout)
{
	if(not _isOpen) throw NotOpen(ERR_MSG_PORT_NOT_OPEN);

	char data[1];
	if( read(data, 1, msTimeout) ) {
		return data[0];
	}

    throw ReadTimeout(ERR_MSG_READ_TIMEOUT);
}
