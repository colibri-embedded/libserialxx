#include <serialxx/base.hpp>

using namespace serialxx;

SerialBase::SerialBase(const std::string& portName, unsigned baudRate, unsigned _to)
	: _isOpen(false)
	, _abort(false)
	, _portName(portName)
	, _baudRate(baudRate)
	, _timeout(_to)
	, _maxStringLength(1024)
	, _rs485mode(RS485mode::OFF)
	, _rs485RtsPolarity(true)
	, _rs485DelayBeforeSend(0)
	, _rs485DelayAfterSend(0)
{
}

SerialBase::SerialBase()
	: SerialBase("")
{
}

SerialBase::SerialBase(const SerialBase& s)
	: _isOpen(s._isOpen)
	, _abort(s._abort)
	, _portName(s._portName)
	, _baudRate(s._baudRate)
	, _timeout(s._timeout)
	, _maxStringLength(s._maxStringLength)
	, _rs485mode(s._rs485mode)
	, _rs485RtsPolarity(s._rs485RtsPolarity)
	, _rs485DelayBeforeSend(s._rs485DelayBeforeSend)
	, _rs485DelayAfterSend(s._rs485DelayAfterSend)
{

}

SerialBase::~SerialBase() 
{

}

bool SerialBase::isOpen()
{
	return _isOpen;
}

void SerialBase::setPortName(const std::string& portName)
{
	_portName = portName;
}

std::string SerialBase::portName()
{
	return _portName;
}

void SerialBase::setBaudRate(unsigned baudRate)
{
	_baudRate = baudRate;
}

unsigned SerialBase::baudRate()
{
	return _baudRate;
}

void SerialBase::setTimeout(unsigned _value)
{
	_timeout = _value;
}

unsigned SerialBase::timeout()
{
	return _timeout;
}

void SerialBase::setMaxStringLength(size_t length)
{
	_maxStringLength = length;
}

size_t SerialBase::maxStringLength()
{
	return _maxStringLength;
}

bool SerialBase::isRS485Supported()
{
	return false;
}

void SerialBase::setRS485Mode(RS485mode mode)
{
	_rs485mode = mode;
}

void SerialBase::setRtsPolarity(bool active_on_send)
{
	_rs485RtsPolarity = active_on_send;
}

void SerialBase::setRS485DelayRtsBeforeSend(unsigned msDelay)
{
	_rs485DelayBeforeSend = msDelay;
}

void SerialBase::setRS485DelayRtsAfterSend(unsigned msDelay)
{
	_rs485DelayAfterSend = msDelay;
}
