#include <serialxx/thread_safe.hpp>

using namespace serialxx;
#include <iostream>

thread_safe::Serial::Serial(const std::string& portName, unsigned baudRate, unsigned timeout)
	: ::serialxx::Serial(portName, baudRate, timeout)
{

}

thread_safe::Serial::Serial()
	: thread_safe::Serial::Serial("")
{

}

thread_safe::Serial::~Serial()
{
	// Test potential dead-lock during descritction
	// in the base close() is called during descruction
}

thread_safe::Serial::Serial(const Serial& s)
    : ::serialxx::Serial(s)
{

}

bool thread_safe::Serial::open()
{
	std::lock_guard<std::mutex> lock(m_control);
	return ::serialxx::Serial::open();
}

void thread_safe::Serial::close()
{
	std::lock_guard<std::mutex> lock(m_control);
	::serialxx::Serial::close();
}

void thread_safe::Serial::abort()
{
	::serialxx::Serial::abort();
}

bool thread_safe::Serial::isOpen()
{
	std::lock_guard<std::mutex> lock(m_control);
	return ::serialxx::Serial::isOpen();
}

void thread_safe::Serial::setPortName(const std::string& portName)
{
	std::lock_guard<std::mutex> lock(m_control);
	::serialxx::Serial::setPortName(portName);
}

std::string thread_safe::Serial::portName()
{
	std::lock_guard<std::mutex> lock(m_control);
	return ::serialxx::Serial::portName();
}

void thread_safe::Serial::setBaudRate(unsigned baudRate)
{
	std::lock_guard<std::mutex> lock(m_control);
	::serialxx::Serial::setBaudRate(baudRate);
}

unsigned thread_safe::Serial::baudRate()
{
	std::lock_guard<std::mutex> lock(m_control);
	return ::serialxx::Serial::baudRate();
}

void thread_safe::Serial::setTimeout(unsigned timeout)
{
	std::lock_guard<std::mutex> lock(m_control);
	::serialxx::Serial::setTimeout(timeout);
}

unsigned thread_safe::Serial::timeout()
{
	std::lock_guard<std::mutex> lock(m_control);
	return ::serialxx::Serial::timeout();
}

void thread_safe::Serial::setMaxStringLength(size_t length)
{
	std::lock_guard<std::mutex> lock(m_control);
	::serialxx::Serial::setMaxStringLength(length);
}

size_t thread_safe::Serial::maxStringLength()
{
	std::lock_guard<std::mutex> lock(m_control);
	return ::serialxx::Serial::maxStringLength();
}

size_t thread_safe::Serial::write(const std::string& data)
{
	return ::serialxx::Serial::write(data);
}

size_t thread_safe::Serial::write(const char* data, size_t count)
{
	std::lock_guard<std::mutex> lock(m_write);
	return ::serialxx::Serial::write(data, count);
}

size_t thread_safe::Serial::readLine(std::string &data, size_t msTimeout, const std::string& eol)
{
	// std::cout << "readLine lock waiting\n" << std::flush;
	std::lock_guard<std::mutex> lock(m_read);
	// std::cout << "readLine lock aquired\n" << std::flush;
	return ::serialxx::Serial::readLine(data, msTimeout, eol);
}

size_t thread_safe::Serial::read(char* data, size_t numberOfBytes, size_t msTimeout)
{
	// std::cout << "read #2 lock waiting\n" << std::flush;
	std::lock_guard<std::mutex> lock(m_read);
	// std::cout << "read #2 lock aquired\n" << std::flush;
	return ::serialxx::Serial::read(data, numberOfBytes, msTimeout);
}
