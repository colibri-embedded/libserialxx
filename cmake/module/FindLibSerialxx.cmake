# This module tries to find libserialxx library and include files
#
# LIBSERIALXX_INCLUDE_DIR, path where to find libwebsockets.h
# LIBSERIALXX_LIBRARY_DIR, path where to find libwebsockets.so
# LIBSERIALXX_LIBRARIES, the library to link against
# LIBSERIALXX_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH ( LIBSERIALXX_INCLUDE_DIR serialxx/serial.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( LIBSERIALXX_LIBRARIES serialxx
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( LIBSERIALXX_LIBRARY_DIR ${LIBSERIALXX_LIBRARIES} PATH )

SET ( LIBSERIALXX_FOUND "NO" )
IF ( LIBSERIALXX_INCLUDE_DIR )
    IF ( LIBSERIALXX_LIBRARIES )
        SET ( LIBSERIALXX_FOUND "YES" )
    ENDIF ( LIBSERIALXX_LIBRARIES )
ENDIF ( LIBSERIALXX_INCLUDE_DIR )

MARK_AS_ADVANCED(
    LIBSERIALXX_LIBRARY_DIR
    LIBSERIALXX_INCLUDE_DIR
    LIBSERIALXX_LIBRARIES
)
