#ifndef SERIAL_UTILS_HPP
#define SERIAL_UTILS_HPP

namespace test {

class SerialBridge {
public:
	static SerialBridge& getInstance() {
		static SerialBridge sb;
		return sb;
	}

	void createSerialPorts();
	void closeSerialPorts();

	virtual ~SerialBridge();

protected:
	SerialBridge();
private:
	static unsigned ref_count;
};

} // namespace test

#endif /* SERIAL_UTILS_HPP */