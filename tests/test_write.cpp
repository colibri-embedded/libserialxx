#include <iostream>
#include <serialxx/serial.hpp>

using namespace serialxx;

int main()
{
	Serial ser("/dev/ttyUSB0");

	try {
		ser.open();
		ser.writeByte('@');

		ser.write("Hello serial");

		const char data[] = {0x04, 0x05, 0x11, 0x78};

		ser.write(data, 4);
	} catch (std::exception& e) {
		std::cout << e.what();
	}
	

	return 0;
}
