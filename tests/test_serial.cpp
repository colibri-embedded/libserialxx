#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <serialxx/serial.hpp>
#include "serial_bridge.hpp"

#include <thread>
#include <string>
#include <iostream>
#include <chrono>
#include <cstring>
#include <functional>

using namespace serialxx;

//// CONSTANTS

constexpr char NO_PORT[] = "/tmp/ttyXYZ999";
constexpr char UART_PORT[] = "/tmp/ttyDUT0";
constexpr char TEST_PORT[] = "/tmp/ttyDUT1";
constexpr unsigned UART_BAUDRATE = 115000;
constexpr unsigned UART_TIMEOUT = 500;
constexpr unsigned SHORT_DELAY = 500;
constexpr unsigned LONG_DELAY = 2000;

#define TIC std::chrono::high_resolution_clock::duration tic_time = std::chrono::high_resolution_clock::now().time_since_epoch()
#define TOC std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()-tic_time).count()

auto bridge = test::SerialBridge::getInstance();

//// HELPERS

class Forward {
	public:
		Forward(const std::string& portName, unsigned baudRate) 
			: m_serial(portName, baudRate, 200)
			, m_running(true)
		{
			m_thread = std::thread(&Forward::loop, this);
		}

		~Forward() {
			{
				std::lock_guard<std::mutex> guard(m_running_mut);
				m_running = false;
			}
			m_thread.join();
		}

		void loop() {
			bool running = false;
			while(true) {
				{
					std::lock_guard<std::mutex> guard(m_running_mut);
					if(not m_running)
						return;
				}

				try {
					std::string data;
					m_serial.read(data);
					m_serial.write(data);
				} catch(...) {

				}

			}
		}

		Serial m_serial;

		bool m_running;
		std::mutex m_running_mut;
		std::thread m_thread;
};

class Delayed {
	public:
		Delayed(unsigned delay, std::function<void()> callback) 
			: m_delay(delay)
			, m_callback(callback)
		{
			
		}

		~Delayed() {
			if(m_thread.joinable())
				m_thread.join();
		}

		void start() {
			m_thread = std::thread(&Delayed::loop, this);
		}

		void loop() {
			std::this_thread::sleep_for( std::chrono::milliseconds(m_delay)  );
			m_callback();
		}

		std::thread m_thread;

		unsigned m_delay;
		std::function<void()> m_callback;
};

//// TESTS

SCENARIO_METHOD(test::SerialBridge, "Unix implementation open/close", "[serial][unix][open-close]") {

	WHEN("Existing port is opened") {
		Serial ser(UART_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( ser.isOpen() );

		THEN("True is returned") {
			REQUIRE( ser.open() );
		}
	}

	WHEN("Existing port is opened") {
		Serial ser(UART_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( ser.isOpen() );

		THEN("No exceptions are thrown") {
			REQUIRE_NOTHROW( ser.open() );
		}
	}

	WHEN("Empty port is specified") {
		Serial ser("", UART_BAUDRATE);
		REQUIRE_FALSE( ser.isOpen() );

		THEN("Exceptions is thrown") {
			REQUIRE_NOTHROW( ser.open() );
			REQUIRE_FALSE( ser.open() );
		}
	}

	WHEN("Non-existing port is opened") {
		Serial ser(NO_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( ser.isOpen() );

		THEN("Exceptions is thrown") {
			REQUIRE_THROWS( ser.open() );
		}
	}

	WHEN("No opened port is closed") {
		Serial ser(UART_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( ser.isOpen() );

		THEN("No exceptions is thrown") {
			REQUIRE_NOTHROW( ser.close() );
		}
	}

	WHEN("Opened port is closed") {
		Serial ser(UART_PORT, UART_BAUDRATE);
		REQUIRE( ser.open() );
		REQUIRE( ser.isOpen() );

		THEN("No exceptions is thrown") {
			REQUIRE_NOTHROW( ser.close() );
			REQUIRE_FALSE( ser.isOpen() );
		}
	}	

}

SCENARIO_METHOD(test::SerialBridge, "Unix implementation read/write", "[serial][unix][read-write]") {

	WHEN("Byte is written") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		REQUIRE( ser0.open() );
		REQUIRE( ser1.open() );

		THEN("Byte is read on the other side") {
			for(unsigned i=0; i<255; i++) {
				REQUIRE( ser0.writeByte( (char)i) == 1 );
				REQUIRE( ser1.readByte() == (char)i );
			}
		}
	}

	WHEN("String is written") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		REQUIRE( ser0.open() );
		REQUIRE( ser1.open() );

		std::string data0 = "this is a test string";
		REQUIRE( ser0.write(data0) == 21 );

		THEN("String is read on the other side") {
			std::string data1;
			size_t len = ser1.read(data1);
			REQUIRE( len == 21 );
			REQUIRE( data1 == data0 );
		}
	}

	WHEN("Char* is written") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		REQUIRE( ser0.open() );
		REQUIRE( ser1.open() );

		char data0[] = "this is a test string";
		REQUIRE( ser0.write(data0) == 21 );

		THEN("Char* is read on the other side") {
			char data1[30];
			size_t len = ser1.read(data1, 30);
			REQUIRE( len == 21 );
			REQUIRE( strcmp(data0, data1) == 0 );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Unix implementation timeout exceptions", "[serial][unix][timeout]") {

	WHEN("No data is written and timeout is specified globally") {
		Serial ser0(UART_PORT, UART_BAUDRATE, UART_TIMEOUT);
		REQUIRE( ser0.open() );

		THEN("readByte throws Timeout exception") {
			TIC;
			REQUIRE_THROWS_AS( ser0.readByte(), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified globally") {
		Serial ser0(UART_PORT, UART_BAUDRATE, UART_TIMEOUT);
		REQUIRE( ser0.open() );

		THEN("read(char*) throws Timeout exception") {
			char data[30];
			TIC;
			REQUIRE_THROWS_AS( ser0.read(data, 30), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified globally") {
		Serial ser0(UART_PORT, UART_BAUDRATE, UART_TIMEOUT);
		REQUIRE( ser0.open() );

		THEN("read(std::string) throws Timeout exception") {
			std::string data;
			TIC;
			REQUIRE_THROWS_AS( ser0.read(data, 30), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified locally") {
		Serial ser0(UART_PORT, UART_BAUDRATE, UART_TIMEOUT);
		REQUIRE( ser0.open() );

		THEN("readLine(std::string) throws Timeout exception") {
			std::string data;
			TIC;
			REQUIRE_THROWS_AS( ser0.readLine(data), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified locally") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		REQUIRE( ser0.open() );

		THEN("readByte throws Timeout exception") {
			TIC;
			REQUIRE_THROWS_AS( ser0.readByte(UART_TIMEOUT), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified locally") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		REQUIRE( ser0.open() );

		THEN("read(char*) throws Timeout exception") {
			char data[30];
			TIC;
			REQUIRE_THROWS_AS( ser0.read(data, 30, UART_TIMEOUT), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified locally") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		REQUIRE( ser0.open() );

		THEN("read(std::string) throws Timeout exception") {
			std::string data;
			TIC;
			REQUIRE_THROWS_AS( ser0.read(data, 30, UART_TIMEOUT), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("No data is written and timeout is specified locally") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		REQUIRE( ser0.open() );

		THEN("readLine(std::string) throws Timeout exception") {
			std::string data;
			TIC;
			REQUIRE_THROWS_AS( ser0.readLine(data, UART_TIMEOUT), ReadTimeout);
			unsigned timeout = TOC;
			REQUIRE( timeout > UART_TIMEOUT-50 );
			REQUIRE( timeout < UART_TIMEOUT+50 );
		}
	}

	WHEN("Data write is delayed") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);
		Delayed delayed(LONG_DELAY, [&ser1](){
			ser1.write("unblock\n");
		});
		REQUIRE( ser0.open() );
		REQUIRE( ser1.open() );

		THEN("readByte blocks until data is received") {
			delayed.start();
			std::string data;
			TIC;
			REQUIRE( ser0.readByte() == 'u' );
			unsigned timeout = TOC;
			REQUIRE( timeout > LONG_DELAY-50 );
			REQUIRE( timeout < LONG_DELAY+50 );
		}
	}

	WHEN("Data write is delayed") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);
		Delayed delayed(LONG_DELAY, [&ser1](){
			ser1.write("unblock\n");
		});

		REQUIRE( ser0.open() );
		REQUIRE( ser1.open() );

		THEN("read blocks until data is received") {
			delayed.start();
			std::string data;
			TIC;
			REQUIRE( ser0.read(data, 30) );
			unsigned timeout = TOC;
			REQUIRE( timeout > LONG_DELAY-50 );
			REQUIRE( timeout < LONG_DELAY+50 );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Unix implementation read/write line", "[serial][unix][read-write") {

	WHEN("A line is written #1") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		ser0.open();
		ser1.open();

		THEN("A line with the ending is read") {
			REQUIRE( ser1.write("test\n") == 5 );
			std::string line;

			REQUIRE( ser0.readLine(line) == 5 );
			REQUIRE( line == "test\n" );
		}

	}

	WHEN("A line is written #2") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		ser0.open();
		ser1.open();

		THEN("A line with the ending is read") {
			REQUIRE( ser1.writeLine("test") == 5 );
			std::string line;

			REQUIRE( ser0.readLine(line) == 5 );
			REQUIRE( line == "test\n" );
		}

	}

	WHEN("A line is written #3") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		ser0.open();
		ser1.open();

		THEN("A line with the ending is read") {
			REQUIRE( ser1.write("te") == 2 );
			REQUIRE( ser1.write("st\n") == 3 );
			std::string line;

			REQUIRE( ser0.readLine(line) == 5 );
			REQUIRE( line == "test\n" );
		}

	}

	WHEN("A line is written with custom EOL") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Serial ser1(TEST_PORT, UART_BAUDRATE);

		ser0.open();
		ser1.open();

		THEN("A line with the custom EOL is read") {
			REQUIRE( ser1.writeLine("test", "\r\n") == 6 );
			std::string line;

			REQUIRE( ser0.readLine(line) == 6 );
			REQUIRE( line == "test\r\n" );
		}

	}

}

SCENARIO_METHOD(test::SerialBridge, "Abort operation", "[serial][unix][abort]") {

	WHEN("Abort is called on a blocking readByte operation") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Delayed delayed(SHORT_DELAY, [&ser0](){
			ser0.abort();
		});

		ser0.open();

		THEN("Read is aborted") {
			delayed.start();
			TIC;

			REQUIRE_THROWS_AS( ser0.readByte(), Aborted );
			unsigned timeout = TOC;
			REQUIRE( timeout > SHORT_DELAY-50 );
			REQUIRE( timeout < SHORT_DELAY+50 );
		}
	}

	WHEN("Abort is called on a blocking read(char*) operation") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Delayed delayed(SHORT_DELAY, [&ser0](){
			ser0.abort();
		});

		ser0.open();

		THEN("Read is aborted") {

			delayed.start();
			TIC;

			char data[30];
			REQUIRE_THROWS_AS( ser0.read(data, 30), Aborted );
			unsigned timeout = TOC;
			REQUIRE( timeout > SHORT_DELAY-50 );
			REQUIRE( timeout < SHORT_DELAY+50 );
		}
	}

	WHEN("Abort is called on a blocking read(std::string) operation") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Delayed delayed(SHORT_DELAY, [&ser0](){
			ser0.abort();
		});

		ser0.open();

		THEN("Read is aborted") {

			delayed.start();
			TIC;

			std::string data;
			REQUIRE_THROWS_AS( ser0.read(data), Aborted );
			unsigned timeout = TOC;
			REQUIRE( timeout > SHORT_DELAY-50 );
			REQUIRE( timeout < SHORT_DELAY+50 );
		}
	}

	WHEN("Abort is called on a blocking readLine operation") {
		Serial ser0(UART_PORT, UART_BAUDRATE);
		Delayed delayed(SHORT_DELAY, [&ser0](){
			ser0.abort();
		});

		ser0.open();

		THEN("Read is aborted") {

			delayed.start();
			TIC;

			std::string data;
			REQUIRE_THROWS_AS( ser0.readLine(data), Aborted );
			unsigned timeout = TOC;
			REQUIRE( timeout > SHORT_DELAY-50 );
			REQUIRE( timeout < SHORT_DELAY+50 );
		}
	}

	WHEN("Abort is called on a non-blocking readLine operation") {
		Serial ser0(UART_PORT, UART_BAUDRATE, 100);
		Delayed delayed(SHORT_DELAY, [&ser0](){
			ser0.abort();
		});

		ser0.open();

		THEN("Read is aborted") {

			delayed.start();
			TIC;

			std::string data;
			REQUIRE_THROWS_AS( ser0.readLine(data, 5000), Aborted );
			unsigned timeout = TOC;
			REQUIRE( timeout > SHORT_DELAY-50 );
			REQUIRE( timeout < SHORT_DELAY+50 );
		}
	}

}
