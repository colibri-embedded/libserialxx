#include <iostream>
#include <serialxx/serial.hpp>

using namespace serialxx;

int main()
{
	Serial ser("/dev/ttyUSB0", 2000000);
	Serial ser1("/dev/ttyUSB1", 2000000);
	ser.setTimeout(1);

	try {
		ser.open();
		//std::string data;
		//auto len = ser.read(data, 100);
		ser1.open();

		ser1.write("hello world of serial");


		/*char data[128];
		auto len = ser.read(data, 128);
		data[len] = 0;

		std::cout << "Len: " << len << std::endl;
		std::cout << "Data: " << data << std::endl;
		*/

		/*char data = ser.readByte(1000);
		std::cout << "Data: " << std::hex << data << std::endl;*/

		std::string data;
		auto len = ser.read(data);
		std::cout << "Len: " << len << std::endl;
		std::cout << "Data: " << data << std::endl;

	} catch (std::exception& e) {
		std::cout << "Exception: " << e.what();
	}
	

	return 0;
}
