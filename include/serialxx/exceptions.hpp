/**
 * MIT License
 * 
 * Copyright (c) 2018 aries-project / software / libraries
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SERIALXX_EXCEPTIONS_HPP
#define SERIALXX_EXCEPTIONS_HPP

#include <string>
#include <stdexcept>
#include <exception>

namespace serialxx {

    /**
     * @brief Error messages utilized when throwing exceptions.
     */
    const std::string ERR_MSG_INVALID_BAUD_RATE      = "Invalid baud rate.";
    const std::string ERR_MSG_READ_TIMEOUT           = "Read timeout";
    const std::string ERR_MSG_PORT_NOT_OPEN          = "Serial port not open.";
    const std::string ERR_MSG_CANNOT_OPEN_PORT       = "Serial port cannot be opened.";
    const std::string ERR_MSG_FD_ATTR_FAIL           = "Failed to set file descriptor attributes.";
    const std::string ERR_MSG_ABORTED                = "Read/Write operation aborted.";
    const std::string ERR_MSG_RS485_CONFIG_FAIL      = "RS485 configuration failure.";

	//invalid_argument
	class NotOpen : public std::logic_error
	{
	    public:
	        /**
	         * @brief Exception error thrown when the serial port is not open.
	         */
	        NotOpen(const std::string& whatArg)
	            : std::logic_error(whatArg)
	        {
	        }
	};

    /**
     * @brief Exception error thrown when the serial port could not be opened.
     */
    class OpenFailed : public std::runtime_error
    {
    public:
        /**
         * @brief Exception error thrown when the serial port could not be opened.
         */
        OpenFailed(const std::string& whatArg)
            : std::runtime_error(whatArg)
        {
        }
    };

    /**
     * @brief Exception error thrown when data could not be read from the
     *        serial port before the timeout had been exceeded.
     */
    class ReadTimeout : public std::runtime_error
    {
    public:
        /**
         * @brief Exception error thrown when data could not be read from the
         *        serial port before the timeout had been exceeded.
         */
        ReadTimeout(const std::string& whatArg)
            : std::runtime_error(whatArg)
        {
        }
    };

    /**
     * @brief Exception error thrown when read/write are aborted by abort()
     *        function.
     */
    class Aborted : public std::runtime_error
    {
    public:
        /**
         * @brief Exception error thrown when read/write are aborted by abort()
        *         function.
         */
        Aborted(const std::string& whatArg)
            : std::runtime_error(whatArg)
        {
        }
    };

    /**
     * @brief Exception error thrown when RS485 parameters configuration
     *        failed.
     */
    class RS485Failure : public std::runtime_error
    {
    public:
        /**
         * @brief Exception error thrown when read/write are aborted by abort()
        *         function.
         */
        RS485Failure(const std::string& whatArg)
            : std::runtime_error(whatArg)
        {
        }
    };

}

#endif /* SERIALXX_EXCEPTIONS_HPP */
