/**
 * MIT License
 * 
 * Copyright (c) 2018 aries-project / software / libraries
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SERIALXX_BASE_HPP
#define SERIALXX_BASE_HPP

#include <string>
#include <cstring>
#include <serialxx/exceptions.hpp>

namespace serialxx {

	enum class RS485mode {
		OFF = 0,
		HARDWARE,
		SOFTWARE
	};

	class SerialBase {
		protected:
			bool _isOpen;
			bool _abort;

			std::string _portName;
			unsigned    _baudRate;
			unsigned	_timeout;

			size_t 		_maxStringLength;

			RS485mode 	_rs485mode;
			bool 		_rs485RtsPolarity;
			unsigned	_rs485DelayBeforeSend;
			unsigned	_rs485DelayAfterSend;
		public:
			SerialBase(const std::string& portName, unsigned baudRate = 9600, unsigned timeout = 0);
			SerialBase();
			SerialBase(const SerialBase&);
			virtual ~SerialBase();

			virtual bool open() =0;
			virtual void close() =0;
			virtual void abort() =0;
			virtual void flush() =0;

			virtual size_t write(const std::string& data) =0;
			virtual size_t write(const char* data, size_t count) =0;
			virtual size_t writeLine(const std::string &data, const std::string& eol="\n") =0;
			virtual size_t writeByte(const char byte) =0;

			virtual size_t read(std::string& data, size_t numberOfBytes=0, size_t msTimeout=0) =0;
			virtual size_t readLine(std::string &data, size_t msTimeout=0, const std::string& eol="\n") =0;
			virtual size_t read(char* data, size_t numberOfBytes, size_t msTimeout=0) =0;
			virtual char   readByte(size_t msTimeout=0) =0;

			virtual void   setRTS(bool value) =0;

			virtual bool isOpen();

			virtual void setPortName(const std::string& portName);
			virtual std::string portName();

			virtual void setBaudRate(unsigned baudRate);
			virtual unsigned baudRate();

			virtual void setTimeout(unsigned timeout);
			virtual unsigned timeout();

			virtual void setMaxStringLength(size_t length);
			virtual size_t maxStringLength();

			/* RS485 */
			virtual bool isRS485Supported();
			virtual void setRS485Mode(RS485mode mode);
			virtual void setRtsPolarity(bool active_on_send);
			virtual void setRS485DelayRtsBeforeSend(unsigned msDelay);
			virtual void setRS485DelayRtsAfterSend(unsigned msDelay);
	};

}

#endif /* SERIALXX_BASE_HPP */
