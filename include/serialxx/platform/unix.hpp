/**
 * MIT License
 * 
 * Copyright (c) 2018 aries-project / software / libraries
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SERIALXX_UNIX_PLATFORM_HPP
#define SERIALXX_UNIX_PLATFORM_HPP

#include <mutex>
#include <serialxx/base.hpp>

namespace serialxx {

	class Serial: public SerialBase {
		private:
			int fd;
			std::mutex m_abort;
		public:
			Serial(const std::string& portName, unsigned baudRate = 9600, unsigned timeout = 0);
			Serial();
			Serial(const Serial&);
			virtual ~Serial();

			bool open() override;
			void close() override;
			void abort() override;
			void flush() override;
			
			size_t write(const std::string& data) override;
			size_t write(const char* data, size_t count) override;
			size_t writeLine(const std::string &data, const std::string& eol="\n") override;
			size_t writeByte(const char byte) override;

			size_t read(std::string& data, size_t numberOfBytes=0, size_t msTimeout=0) override;
			size_t readLine(std::string &data, size_t msTimeout=0, const std::string& eol="\n") override;
			size_t read(char* data, size_t numberOfBytes, size_t msTimeout=0);
			char   readByte(size_t msTimeout=0) override;

			void setRTS(bool value) override;

			bool isRS485Supported() override;
	};

}

#endif /* SERIALXX_UNIX_PLATFORM_HPP */
